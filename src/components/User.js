import React, { Component } from "react";
import "./User.css";
import axios from "axios";
import Error from "./Error";
export default class Users extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: null,
      received: false,
      idError: false,
    };
  }
  componentDidMount() {
    const id = this.props.match.params.id;
    if (id <= 0 || id > 10 || isNaN(id)) {
      this.setState({
        idError: true,
      });
      return;
    }

    axios
      .get(`https://jsonplaceholder.typicode.com/users/${id}`)
      .then((res) => {
        const users = res.data;
        this.setState({ user: users, received: true, idError: false });
      });
  }
  render() {
    const { user, received, idError } = { ...this.state };

    return (
      <div>
        {idError ? (
          <Error />
        ) : this.state.received ? (
          <div>
            <div className="main-container">
              <div className="user-container">
                <div class="user-heading">
                  <div className="name-heading">
                    <div className="icon-1">
                      <i class="fa fa-user"></i>
                    </div>
                    <div className="user">
                      <h2>
                        {user.name} @ {user.username}
                      </h2>
                    </div>
                  </div>
                  <div className="internal-container">
                    <div className="user-content">
                      <div className="item-heading">
                        <i class="fa fa-suite"></i>
                        {user.address.street}, {user.address.suite},{" "}
                        {user.address.city}-{user.address.zipcode}
                      </div>

                      <div className="item-heading-company">
                        <i class="fa fa-company"> </i>
                        <div>
                          {user.company.name}, {user.company.catchPhrase}
                        </div>
                      </div>
                      <div className="contact">
                        <div className="item item-1">
                          <div>
                            <i class="fa fa-phone"></i>
                          </div>
                          {user.phone}
                        </div>
                        <div className="item item-2">
                          <div>
                            <i class="fa fa-email"></i>
                          </div>
                          {user.email}
                        </div>
                        <div className="item item-3">
                          <div>
                            <i class="fa fa-web"></i>
                          </div>
                          {user.website}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ) : null}
      </div>
    );
  }
}
