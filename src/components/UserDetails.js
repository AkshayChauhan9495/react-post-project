import React from "react";
import "./UserDetails.css";
import { Link } from "react-router-dom";
import axios from "axios";
class UserDetails extends React.Component {
  state = {
    users: [],
    posts: [],
    comments: [],
    commentRender: false,
    commentArr: Array(101).fill(false),
  };

  componentDidMount() {
    axios.get(`https://jsonplaceholder.typicode.com/users`).then((res) => {
      const users = res.data;
      this.setState({ users });
    });

    axios.get(`https://jsonplaceholder.typicode.com/posts`).then((res) => {
      const posts = res.data;
      this.setState({ posts });
    });
    axios.get(`https://jsonplaceholder.typicode.com/comments`).then((res) => {
      const comments = res.data;
      this.setState({ comments });
    });
  }

  commentRenderFn(id) {
    const temp = this.state.commentArr;
    temp[id] = !temp[id];
    this.setState({ commentArr: temp });
  }

  render() {
    return (
      <div className="container">
        <div className="">
          <div className="collection">
            {this.state.users.map((user) => (
              <div className="main-container">
                <div key={user.id} className=""></div>
                <div className="">
                  <div className="collection">
                    {this.state.posts.map((post) => {
                      if (post.userId === user.id) {
                        return (
                          <div key={post.userId} className="">
                            <div className="blog">
                              <div className="header-class">
                                <div className="icons">
                                  <i class="fa fa-user"></i>
                                  <div>
                                    <Link
                                      className="user-name"
                                      to={`/user/${user.id}`}
                                    >
                                      @{user.username}
                                    </Link>
                                    <p className="name">{user.name}</p>
                                  </div>
                                </div>
                              </div>
                              <Link
                                to={`/post/${post.id}`}
                                className="middle-section user-name"
                              >
                                <p className="title">{post.title}</p>
                                <p>{post.body}</p>
                              </Link>
                              <div className="btn-container">
                                <div
                                  className="btn"
                                  onClick={() => {
                                    this.commentRenderFn(post.id);
                                  }}
                                >
                                  <i class="fa fa-comment"> 5 comments</i>
                                </div>
                              </div>
                            </div>
                            {this.state.commentArr[post.id]
                              ? this.state.comments.map((comment) => {
                                  if (comment.postId === post.id) {
                                    return (
                                      <div className="comment-container">
                                        <div className="comment-name">
                                          Name: {comment.name}
                                        </div>
                                        <div className="comment-email">
                                          Email: {comment.email}
                                        </div>
                                        <div className="comment-body">
                                          Body: {comment.body}
                                        </div>
                                      </div>
                                    );
                                  }
                                  return undefined;
                                })
                              : undefined}
                          </div>
                        );
                      }
                      return undefined;
                    })}
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    );
  }
}
export default UserDetails;
