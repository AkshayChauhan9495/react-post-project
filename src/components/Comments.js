import React, { Component } from "react";
class Comments extends Component {
  constructor(props) {
    super(props);
  }

  countComments = (id) => {
    let countComment = 0;
    const commentPost = this.props.comments.filter((value) => {
      return value.postId == id;
    });
    countComment = Object.keys(commentPost).length;

    return countComment;
  };
  render() {
    return (
      <div>
        <div className="icon-div">
          <div>
            <div className="icon">
              <i class="fa fa-comment"></i>
              <div>{this.countComments(this.props.postId)} Comments</div>{" "}
            </div>
          </div>
        </div>
        {this.props.comments.map((value) => {
          if (value.postId == this.props.postId) {
            console.log(value.id);
            return (
              <div key={value.id} className="comment">
                <div className="text">
                  <div className="user-icon"></div>
                  <div className="name-email">
                    <div>{value.name} </div>
                    <div>{value.email}</div>
                  </div>
                </div>
                <div className="comment-body">{value.body}</div>
              </div>
            );
          }
        })}
      </div>
    );
  }
}

export default Comments;
