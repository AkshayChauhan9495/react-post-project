import React, { Component } from "react";
import Comments from "./Comments";
// import axios from "axios";
import "./Post.css";
import Error from "./Error";
import Loader from "./Loader";

export default class Post extends Component {
  constructor(props) {
    super(props);
    this.state = {
      post: null,
      user: null,
      comments: null,
      received: false,
      postId: null,
      errorId: false,
      isLoaded: false,
    };
  }

  componentDidMount = () => {
    const id = this.props.match.params.id;

    if (id <= 0 || id > 100 || isNaN(id)) {
      this.setState({
        errorId: true,
      });
      return;
    }
    let post = null;
    let user = null;
    let postId = null;

    // axios
    //   .get(`https://jsonplaceholder.typicode.com/posts/${id}`)
    //   .then((res) => {
    //     const posts = res.data;
    //     post = posts;
    //     postId = posts.id;
    //     // this.setState({ post: posts, postId: posts.id });
    //   });

    // axios
    //   .get(`https://jsonplaceholder.typicode.com/users/${post.userId}`)
    //   .then((res) => {
    //     const users = res.data;
    //     user = users;
    //     // this.setState({ user: users });
    //   });
    // axios.get(`https://jsonplaceholder.typicode.com/comments`).then((res) => {
    //   const comment = res.data;
    //   this.setState({
    //     post: post,
    //     user: user,
    //     comments: comment,
    //     postId: postId,
    //     received: true,
    //   });
    // });

    fetch(`https://jsonplaceholder.typicode.com/posts/${id}`)
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        post = data;
        postId = data.id;

        return fetch(
          `https://jsonplaceholder.typicode.com/users/${post.userId}`
        );
      })
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        console.log(data);
        user = data;

        return fetch("https://jsonplaceholder.typicode.com/comments");
      })
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        this.setState({
          post: post,
          user: user,
          comments: data,
          postId: postId,
          received: true,
          isLoaded: true,
        });
      });
  };
  render() {
    const { post, user, comments, received, postId, errorId } = {
      ...this.state,
    };

    return (
      <div>
        {this.state.isLoaded === false ? (
          <Loader />
        ) : (
          <div>
            {errorId ? (
              <Error />
            ) : this.state.received ? (
              <div key={postId} className="main-container">
                <div className="post-contain">
                  <div key={user.id}>
                    <div className="post-details">
                      <div className="user-icon">
                        <i class="fa fa-user"></i>
                      </div>
                      <div className="user-name">
                        <div className="padding">@{user.username}</div>
                        <div>{user.name}</div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="post-container">
                  <div>
                    <h2>{post.title}</h2>
                  </div>
                  <div>{post.body}</div>
                </div>
                <div className="comment-container">
                  <Comments postId={postId} comments={comments} />
                </div>
              </div>
            ) : null}
          </div>
        )}
      </div>
    );
  }
}
