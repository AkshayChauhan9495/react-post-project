import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Post from "./components/Post";
import User from "./components/User";
import UserDetails from "./components/UserDetails";

class App extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Router>
        <div className="App">
          <Switch>
            <Route exact path="/" component={UserDetails} />
            <Route exact path="/post/:id" component={Post} />
            <Route exact path="/user/:id" component={User} />
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
